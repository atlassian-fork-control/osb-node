/**
 * OSB Unbind Service Request
 */
class UnbindServiceRequest {
  public instanceId: string;
  public bindingId: string;
  public serviceId: string;
  public planId: string;

  public constructor({instance_id, binding_id, service_id, plan_id}) {
    this.instanceId = instance_id;
    this.bindingId = binding_id;
    this.serviceId = service_id;
    this.planId = plan_id;
  }
}

export = UnbindServiceRequest;
