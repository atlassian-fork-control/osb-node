import BindResource = require('./BindResource');

/**
 * OSB Bind Service Request
 */
class BindServiceRequest {
  public instanceId: string;
  public bindingId: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public context: any;
  public serviceId: string;
  public planId: string;
  public appGuid: string;
  public bindResource: BindResource;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public parameters: any;

  public constructor({instance_id, binding_id, context, service_id,
              plan_id, app_guid, bind_resource, parameters}) {
    this.instanceId = instance_id;
    this.bindingId = binding_id;
    this.context = context;
    this.serviceId = service_id;
    this.planId = plan_id;
    this.appGuid = app_guid;
    if (bind_resource) {
      this.bindResource = new BindResource(bind_resource);
    }
    this.parameters = parameters;
  }
}

export = BindServiceRequest;
