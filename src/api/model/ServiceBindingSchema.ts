/**
 * OSB Service Bindings Object
 */
import InputParameters = require('./InputParameters');

class ServiceBindingSchema {
  public create: InputParameters;

  public constructor({create}: {create: InputParameters}) {
    if (create) {
      this.create = new InputParameters(create);
    }
  }
}

export = ServiceBindingSchema;
