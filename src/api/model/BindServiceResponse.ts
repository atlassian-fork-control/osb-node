/**
 * OSB Bind Service Response
 */
import VolumeMount = require('./VolumeMount');

class BindServiceResponse {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public credentials: any;
  public syslog_drain_url: string;
  public route_service_url: string;
  public volume_mounts: VolumeMount[];

  public constructor({credentials, syslog_drain_url, route_service_url, volume_mounts}:
                         // eslint-disable-next-line @typescript-eslint/no-explicit-any
                         {credentials: any, syslog_drain_url: string, route_service_url: string,
                           volume_mounts: any[]}) {
    this.credentials = credentials;
    this.syslog_drain_url = syslog_drain_url;
    this.route_service_url = route_service_url;
    this.volume_mounts = volume_mounts.map(v => new VolumeMount(v));
  }
}

export = BindServiceResponse;
