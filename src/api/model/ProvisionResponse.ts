/**
 * OSB Provision Response Object
 */
class ProvisionResponse {
  public dashboard_url: string;
  public operation: string;

  public constructor({dashboard_url, operation}) {
    this.dashboard_url = dashboard_url;
    this.operation = operation;
  }
}

export = ProvisionResponse;
