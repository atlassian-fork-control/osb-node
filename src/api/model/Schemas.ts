/**
 * OSB Schema object
 */
import ServiceInstanceSchema = require('./ServiceInstanceSchema');
import ServiceBindingSchema = require('./ServiceBindingSchema');

class Schemas {
  public service_instance?: ServiceInstanceSchema;
  public service_binding?: ServiceBindingSchema;

  public constructor({service_instance, service_binding}) {
    if (service_instance) {
      this.service_instance = new ServiceInstanceSchema(service_instance);
    }
    if (service_binding) {
      this.service_binding = new ServiceBindingSchema(service_binding);
    }
  }
}

export = Schemas;
