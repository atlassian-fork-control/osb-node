/**
 * OSB Service Object
 */
import DashboardClient = require('./DashboardClient');
import Plan = require('./Plan');
import ServiceMetadata = require('./ServiceMetadata');

class Service {
  public name: string;
  public id: string;
  public description: string;
  public tags: string[];
  public requires: string[];
  public bindable: boolean;
  public instances_retrievable: boolean;
  public bindings_retrievable: boolean;
  public metadata?: ServiceMetadata;
  public dashboard_client?: DashboardClient;
  public plan_updateable: boolean;
  public plans: Plan[];

  public constructor({ name, id, description, tags, requires = [],
    bindable, metadata, dashboard_client, plan_updateable = false, plans,
                     instances_retrievable = false, bindings_retrievable = false}) {

    this.name = name;
    this.id = id;
    this.description = description;
    this.tags = tags;
    this.requires = requires;
    this.bindable = bindable;
    if (metadata) {
      this.metadata = new ServiceMetadata(metadata);
    }
    if (dashboard_client) {
      this.dashboard_client = new DashboardClient(dashboard_client);
    }
    this.plan_updateable = plan_updateable;
    if (plans) {
      this.plans = plans.map(p => new Plan(p));
    }
    this.instances_retrievable = instances_retrievable;
    this.bindings_retrievable = bindings_retrievable;
  }
}

export = Service;
