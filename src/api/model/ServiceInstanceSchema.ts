/**
 * OSB Service Instances Object
 */
import InputParameters = require('./InputParameters');

class ServiceInstanceSchema {
  public create: InputParameters;
  public update: InputParameters;

  public constructor({create, update}: {create: InputParameters, update: InputParameters}) {
    if (create) {
      this.create = new InputParameters(create);
    }
    if (update) {
      this.update = new InputParameters(update);
    }
  }
}

export = ServiceInstanceSchema;
