/* eslint-disable no-unused-vars */
/**
 * Base OSB service class which handles OSB requests.
 */
import ProvisionResponse = require('./model/ProvisionResponse');
import ProvisionRequest = require('./model/request/ProvisionRequest');
import UpdateRequest = require('./model/request/UpdateRequest');
import DeprovisionRequest = require('./model/request/DeprovisionRequest');
import BindServiceRequest = require('./model/request/BindServiceRequest');
import BindServiceResponse = require('./model/BindServiceResponse');
import UnbindServiceRequest = require('./model/request/UnbindServiceRequest');
import LastOperationRequest = require('./model/request/LastOperationRequest');
import LastOperation = require('./model/LastOperation');
import Service = require('./model/Service');
import UnbindResponse = require('./model/UnbindResponse');
import DeprovisionResponse = require('./model/DeprovisionResponse');
import StatusCodeWrapper = require('./StatusCodeWrapper');

type ContextType = any;

type getServiceInfoType = (context?: ContextType) => Promise<Service>;
type provisionServiceInstanceType = (request: ProvisionRequest, context: ContextType) => Promise<StatusCodeWrapper<ProvisionResponse>>;
type updateServiceInstanceType = (request: UpdateRequest, context: ContextType) => Promise<StatusCodeWrapper<Service>>;
type deprovisionServiceInstanceType = (request: DeprovisionRequest, context: ContextType) => Promise<StatusCodeWrapper<DeprovisionResponse>>;
type bindServiceType = (request: BindServiceRequest, context: ContextType) => Promise<StatusCodeWrapper<BindServiceResponse>>;
type unbindServiceType = (request: UnbindServiceRequest, context: ContextType) => Promise<StatusCodeWrapper<UnbindResponse>>;
type pollLastOperationType = (request: LastOperationRequest, context: ContextType) => Promise<StatusCodeWrapper<LastOperation>>;

class OSBService {
  public getServiceInfo: getServiceInfoType;
  public provisionServiceInstance: provisionServiceInstanceType;
  public updateServiceInstance: updateServiceInstanceType;
  public deprovisionServiceInstance: deprovisionServiceInstanceType;
  public bindService: bindServiceType;
  public unbindService: unbindServiceType;
  public pollLastOperation: pollLastOperationType;

  /**
   *
   * @param getServiceInfo async function to get service info
   * @param provisionServiceInstance async function to provision service instance
   * @param updateServiceInstance async function to update service instance
   * @param deprovisionServiceInstance async function to deprovision service instance
   * @param bindService async function to bind service
   * @param unbindService async function to unbind service
   * @param pollLastOperation async function to get last operation status
   */
  public constructor({getServiceInfo, provisionServiceInstance, updateServiceInstance, deprovisionServiceInstance,
                     bindService, unbindService, pollLastOperation}: {
    getServiceInfo: getServiceInfoType;
    provisionServiceInstance: provisionServiceInstanceType;
    updateServiceInstance: updateServiceInstanceType;
    deprovisionServiceInstance: deprovisionServiceInstanceType;
    bindService: bindServiceType;
    unbindService: unbindServiceType;
    pollLastOperation: pollLastOperationType;
  }) {
    this.getServiceInfo = getServiceInfo;
    this.provisionServiceInstance = provisionServiceInstance;
    this.updateServiceInstance = updateServiceInstance;
    this.deprovisionServiceInstance = deprovisionServiceInstance;
    this.bindService = bindService;
    this.unbindService = unbindService;
    this.pollLastOperation = pollLastOperation;
  }
}

export = OSBService;
