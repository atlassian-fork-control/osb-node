

// eslint-disable-next-line node/no-missing-require
const hooks = require('hooks');
const chai = require('chai');
const should = chai.should();
const before = hooks.before;
const after = hooks.after;

const testData = require('../test-data');

// --- before hooks ---
before('service_instances > /v2/service_instances/{instance_id} > Provisions a service instance > 400 > application/json; charset=utf-8', function (transaction) {
  transaction.skip = false;
});

before('service_instances > /v2/service_instances/{instance_id} > Provisions a service instance > 409 > application/json; charset=utf-8', function (transaction) {
  transaction.skip = false;
});

before('service_instances > /v2/service_instances/{instance_id} > Provisions a service instance > 422 > application/json; charset=utf-8', function (transaction) {
  transaction.skip = false;
});

// --- after hooks - validation ---
after('service_instances > /v2/service_instances/{instance_id} > Provisions a service instance > 200 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.PROVISION_RESPONSE));
});

after('service_instances > /v2/service_instances/{instance_id} > Provisions a service instance > 201 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.PROVISION_RESPONSE));
});

after('service_instances > /v2/service_instances/{instance_id} > Provisions a service instance > 202 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.OPERATION_IN_PROGRESS_RESPONSE));
});

after('service_instances > /v2/service_instances/{instance_id} > Provisions a service instance > 400 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.BAD_REQUEST));
});

after('service_instances > /v2/service_instances/{instance_id} > Provisions a service instance > 409 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify(testData.EMPTY_RESPONSE));
});

after('service_instances > /v2/service_instances/{instance_id} > Provisions a service instance > 422 > application/json; charset=utf-8', function (transaction) {
  transaction.real.body.should.deep.equals(JSON.stringify({description: 'broker only supports asynchronous provisioning for the requested plan'}));
});
